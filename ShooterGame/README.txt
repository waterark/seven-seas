#####################################
#   |\| /\ < |-| ( )  - 12D Gaming #
#####################################


**DO NOT EDIT - serverGrid.Json in MapEditor** 

Please do not edit the serverGrid.Json file within the MapEditor, as it deletes the Powerstone Spawn point settings...
This will remove the quest line, only edit in a text editor!

You will need to add your Ports and Servername in Via the text editor (Notepad ++).
each grid is named by A1 - SPAWN, A2, A3, B1, B2, B3, C1, C2, C3

Happy Sailing!

**DO NOT EDIT - serverGrid.Json in MapEditor** 

#####################################
#   Created by NaCHO  - 12D Gaming #
#####################################