
## Resources

* Gold Coin `cheat gfi coin 100 0 0`
* Mythos `cheat gfi marrow 100 0 0`

### Coal

* Coal (Base) `cheat gfi coal_base 100 0 0`
* Anthracite (Coal) `cheat gfi anthracite 100 0 0`
* Graphite (Coal) `cheat gfi graphite 100 0 0`
* Lignite (Coal) `cheat gfi lignite 100 0 0`
* Nitre (Coal) `cheat gfi nitre 100 0 0`
* Sulfur (Coal) `cheat gfi coal_sulfur 100 0 0`
* Coral (Coal) `cheat gfi coral 100 0 0`
* Brain Coral (Coal) `cheat gfi brain 100 0 0`
* Fire Coral (Coal) `cheat gfi coral_fire 100 0 0`

### Crystal

* Crystal (Base) `cheat gfi crystal_base 100 0 0`
* Calcite (Crystal) `cheat gfi calcite 100 0 0`
* Herkimer (Crystal) `cheat gfi herkimer 100 0 0`
* Pearl (Crystal) `cheat gfi crystal_pearl 100 0 0`
* Quartz (Crystal) `cheat gfi quartz 100 0 0`
* Tellurite (Crystal) `cheat gfi tellurite 100 0 0`
* Amethyst (Crystal) `cheat gfi amethyst 100 0 0`

### Fibers

* Fibers (Base) `cheat gfi fibers_base 100 0 0`
* Bamboo (Fibers) `cheat gfi bamboo 100 0 0`
* Cotton (Fibers) `cheat gfi cotton 100 0 0`
* Hemp (Fibers) `cheat gfi hemp 100 0 0`
* Jute (Fibers) `cheat gfi jute 100 0 0`
* Seaweed (Fibers) `cheat gfi seaweed 100 0 0`
* Silk (Fibers) `cheat gfi fibers_silk 100 0 0`
* Straw (Fibers) `cheat gfi straw 100 0 0`
* Salve (Fibers) `cheat gfi salve 100 0 0`

### Flint

* Flint (Base) `cheat gfi flint_base 100 0 0`
* Agate (Flint) `cheat gfi agate 100 0 0`
* Basalt (Flint) `cheat gfi basalt 100 0 0`
* Chalcedony (Flint) `cheat gfi chalcedony 100 0 0`
* Chert (Flint) `cheat gfi chert 100 0 0`
* Obsidian (Flint) `cheat gfi flint_obsidian 100 0 0`
* Radiolarite (Flint) `cheat gfi radiolarite 100 0 0`

### Gem

* Gem (Base) `cheat gfi gem_base 100 0 0`
* Diamond (Gem) `cheat gfi diamond 100 0 0`
* Emerald (Gem) `cheat gfi emerald 100 0 0`
* Garnet (Gem) `cheat gfi garnet 100 0 0`
* Opal (Gem) `cheat gfi opal 100 0 0`
* Ruby (Gem) `cheat gfi ruby 100 0 0`
* Sunstone (Gem) `cheat gfi sunstone 100 0 0`

### Hide

* Hide (Base) `cheat gfi hide_base 100 0 0`
* Fur (Hide) `cheat gfi hide_fur 100 0 0`
* Hair (Hide) `cheat gfi hide_hair 100 0 0`
* Leather (Hide) `cheat gfi hide_leather 100 0 0`
* Pelt (Hide) `cheat gfi hide_pelt 100 0 0`
* Skin (Hide) `cheat gfi hide_skin 100 0 0`
* Fleece (Hide) `cheat gfi hide_wool 100 0 0`

### Keratanoid

* Keratanoid (Base) `cheat gfi keratanoid_base 100 0 0`
* Bone (Keratanoid) `cheat gfi keratanoid_bone 100 0 0`
* Carapace (Keratanoid) `cheat gfi carapace 100 0 0`
* Chitin (Keratanoid) `cheat gfi keratanoid_chitin 100 0 0`
* Residue (Keratanoid) `cheat gfi residue 100 0 0`
* Scale (Keratanoid) `cheat gfi scale 100 0 0`
* Shell (Keratanoid) `cheat gfi shell 100 0 0`

### Metal

* Metal (Base) `cheat gfi metal_base 100 0 0`
* Cobalt (Metal) `cheat gfi metal_cobalt 100 0 0`
* Copper (Metal) `cheat gfi metal_copper 100 0 0`
* Iridium (Metal) `cheat gfi metal_iridium 100 0 0`
* Iron (Metal) `cheat gfi metal_iron 100 0 0`
* Silver (Metal) `cheat gfi metal_silver 100 0 0`
* Tin (Metal) `cheat gfi metal_tin 100 0 0`

### Oil

* Oil (Base) `cheat gfi oil_base 100 0 0`
* Blubber (Oil) `cheat gfi blubber 100 0 0`
* Crude (Oil) `cheat gfi crude 100 0 0`
* Fish (Oil) `cheat gfi oil_fish 100 0 0`
* Mineral (Oil) `cheat gfi mineral 100 0 0`
* Naptha (Oil) `cheat gfi naptha 100 0 0`
* Shale Oil (OliveOil) `cheat gfi oil_olive 100 0 0`

### Salt

* Salt (Base) `cheat gfi preservative 100 0 0`
* FlakeSalt (Salt) `cheat gfi flakesalt 100 0 0`
* Iodine (Salt) `cheat gfi iodine 100 0 0`
* KalaNamak (Salt) `cheat gfi kalanamak 100 0 0`
* PinkSalt (Salt) `cheat gfi pinksalt 100 0 0`
* RockSalt (Salt) `cheat gfi rocksalt 100 0 0`
* SeaSalt (Salt) `cheat gfi seasalt 100 0 0`

### Sap

* Sap (Base) `cheat gfi sap_base 100 0 0`
* Honey (Sap) `cheat gfi sap_honey 100 0 0`
* Gum (Sap) `cheat gfi latex 100 0 0`
* Nectar (Sap) `cheat gfi nectar 100 0 0`
* Resin (Sap) `cheat gfi resin 100 0 0`
* Syrup (Sap) `cheat gfi syrup 100 0 0`
* Sugars (Sap) `cheat gfi wax 100 0 0`

### Stone

* Stone (Base) `cheat gfi stone_base 100 0 0`
* Coquina (Stone) `cheat gfi coquina 100 0 0`
* Granite (Stone) `cheat gfi granite 100 0 0`
* Limestone (Stone) `cheat gfi limestone 100 0 0`
* Marble (Stone) `cheat gfi marble 100 0 0`
* Sandstone (Stone) `cheat gfi sandstone 100 0 0`
* Slate (Stone) `cheat gfi stone_slate 100 0 0`

### Thatch

* Thatch (Base) `cheat gfi thatch_base 100 0 0`
* Bark (Thatch) `cheat gfi bark 100 0 0`
* Frond (Thatch) `cheat gfi frond 100 0 0`
* Reed (Thatch) `cheat gfi reed 100 0 0`
* Root (Thatch) `cheat gfi root 100 0 0`
* Rush (Thatch) `cheat gfi thatch_rush 100 0 0`
* Twig (Thatch) `cheat gfi twig 100 0 0`

### Wood

* Wood (Base) `cheat gfi wood_base 100 0 0`
* Ash (Wood) `cheat gfi ash 100 0 0`
* Cedar (Wood) `cheat gfi cedar 100 0 0`
* Fir (Wood) `cheat gfi wood_fir 100 0 0`
* Ironwood (Wood) `cheat gfi ironwood 100 0 0`
* Oak (Wood) `cheat gfi oak 100 0 0`
* Pine (Wood) `cheat gfi pine 100 0 0`
* Poplar (Wood) `cheat gfi poplar 100 0 0`

### Refined Resources

* Blasting Powder `cheat gfi blasting 100 0 0`
* Organic Paste `cheat gfi organicpaste 100 0 0`
* Organic Paste (Coral) `cheat gfi powderedcoral 100 0 0`
* Fire Gel `cheat gfi firegel 100 0 0`
* Gunpowder `cheat gfi gunpowder 100 0 0`
* Preserving Salt `cheat gfi preservingsalt 100 0 0`
* Alloy `cheat gfi ingot_base 100 0 0`
* Cobalt Ingot (Alloy) `cheat gfi cobalt 100 0 0`
* Copper Ingot (Alloy) `cheat gfi copper 100 0 0`
* Iridum Ingot (Alloy) `cheat gfi iridium 100 0 0`
* Iron Ingot (Alloy) `cheat gfi iron 100 0 0`
* Silver Ingot (Alloy) `cheat gfi ingot_silver 100 0 0`
* Tin Ingot (Alloy) `cheat gfi ingot_tin 100 0 0`

## Consumables

### Cooking

* Ale `cheat gfi consumable_ale 1 0 0`
* Jac's Beef Buns `cheat gfi beefbuns 1 0 0`
* Berry Tea `cheat gfi berrytea 1 0 0`
* Amy's Bubble N'Squeak `cheat gfi consumable_bubblensqueak 1 0 0`
* Colton's Celery Soup `cheat gfi celerysoup 1 0 0`
* Cooked Fish `cheat gfi cookedfish 1 0 0`
* Cooked Meat `cheat gfi cookedmeat 1 0 0`
* Jat's Creme Brulee `cheat gfi cremebrulee 1 0 0`
* Drake's Dark Draught `cheat gfi darkdraught 1 0 0`
* Dragon's Tongue Delight `cheat gfi dragonstongue 1 0 0`
* Lucy's Fish 'n Chips `cheat gfi fishnchips 1 0 0`
* Grog `cheat gfi grog 1 0 0`
* Hardtack `cheat gfi hardtack 1 0 0`
* Hot Cocoa `cheat gfi hotchocolate 1 0 0`
* Monarch's Cake `cheat gfi monarchscake 1 0 0`
* Pork Pie `cheat gfi porkpie 1 0 0`
* Debby's Pudding `cheat gfi pudding 1 0 0`
* Reaper's Regard `cheat gfi reapersregard 1 0 0`
* Alex's Rosemary Chicken `cheat gfi rosemarychicken 1 0 0`
* Navin's Shrunken Head Stew `cheat gfi shrunkenstew 1 0 0`
* Song of the Sea `cheat gfi songofthesea 1 0 0`
* Robert's Spiced Rum `cheat gfi spicedrum 1 0 0`
* Iso's Spicy Roll `cheat gfi spicyroll 1 0 0`
* Steven's Stuffed & Baked Fish `cheat gfi stuffedbakedfish 1 0 0`
* Tara's Nosti Treat `cheat gfi taranostitreat 1 0 0`
* Delicacy `cheat gfi delicacy 1 0 0`
* Unthinkable Delicacy `cheat gfi unthinkabledelicacy 1 0 0`

### Eggs

* Chicken Egg `cheat gfi chicken 1 0 0`
* Fertilized Chicken Egg `cheat gfi chicken_fertilized 1 0 0`
* Crow Egg `cheat gfi crow 1 0 0`
* Fertilized Crow Egg `cheat gfi crow_fertilized 1 0 0`
* Mockatoo (Parrot) Egg `cheat gfi parrot 1 0 0`
* Fertilized Mockatoo (Parrot) Egg `cheat gfi parrot_fertilized 1 0 0`
* Seagull Egg `cheat gfi seagull 1 0 0`
* Fertilized Seagull Egg `cheat gfi seagull_fertilized 1 0 0`

### Meat

* Cooked Prime Animal Meat `cheat gfi cookedprimeanimal 1 0 0`
* Cooked Prime Fish Meat `cheat gfi cookedprimefish 1 0 0`
* Salted Fish `cheat gfi saltedfish 1 0 0`
* Salted Meat `cheat gfi saltedmeat 1 0 0`
* Fish Meat `cheat gfi meat_fish 1 0 0`
* Prime Fish Meat `cheat gfi primefish 1 0 0`
* Animal Meat `cheat gfi animal 1 0 0`
* Prime Animal Meat `cheat gfi primeanimal 1 0 0`
* Earthworms `cheat gfi worm 1 0 0`
* Marrow `cheat gfi meat_marrow 1 0 0`

## Farming

### Crops

* Beans `cheat gfi beans 1 0 0`
* Beet `cheat gfi beet 1 0 0`
* Cactus `cheat gfi cactus 1 0 0`
* Chick Peas `cheat gfi chickpeas 1 0 0`
* Edible Greens `cheat gfi ediblegreens 1 0 0`
* Maize `cheat gfi maize 1 0 0`
* Onion `cheat gfi onion 1 0 0`
* Pepper `cheat gfi pepper 1 0 0`
* Potato	`cheat gfi potato 1 0 0`
* Rice `cheat gfi rice 1 0 0`
* Turnip `cheat gfi turnip 1 0 0`
* Wheat `cheat gfi wheat 1 0 0`

### Seeds

* Veggie Seed `cheat gfi seed_base 1 0 0`
* Bean Seed `cheat gfi beans_seed 1 0 0`
* Beet Seed `cheat gfi beet_seed 1 0 0`
* Cactus Seed `cheat gfi cactus_seed 1 0 0`
* Carrot Seed `cheat gfi carrot_seed 1 0 0`
* Chick Peas Seed `cheat gfi chickpeas_seed 1 0 0`
* Chilli Seed `cheat gfi chilli_seed 1 0 0`
* Edible Greens Seed `cheat gfi ediblegreens_seed 1 0 0`
* Maize Seed `cheat gfi maize_seed 1 0 0`
* Onion Seed `cheat gfi onion_seed 1 0 0`
* Pepper Seed `cheat gfi pepper_seed 1 0 0`
* Potato Seed `cheat gfi potato_seed 1 0 0`
* Rice Seed `cheat gfi rice_seed 1 0 0`
* Turnip Seed `cheat gfi turnip_seed 1 0 0`
* Wheat Seed `cheat gfi wheat_seed 1 0 0`

## Structures

* Water Pipe System `cheat gfi pipe_water_stone 1 0 0`
* Stone Water Reservoir `cheat gfi waterreservoir_stone 1 0 0`
* Small Crop Plot `cheat gfi cropplot_s 1 0 0`
* Large Crop Plot `cheat gfi cropplot_l 1 0 0`
* Fertilizer `cheat gfi fertilizer_compost 1 0 0`

## Items (Weapon)

### Firearms

* Flintlock `cheat gfi pistol 1 0 0`
* Simple Bullet `cheat gfi bullet 1 0 0`
* Blunderbuss `cheat gfi blunderbuss 1 0 0`
* Simple Shot `cheat gfi shotgun 1 0 0`
* Carbine `cheat gfi sniperrifle 1 0 0`
* Minni Ball `cheat gfi minni 1 0 0`

< in progress >

## Items (Armor)

< in progress >

## Structures (Building)

## Useful

* Structure `Cheat Box - cheat gfi cheatbox 1 0 0`
* Color `Cheat Box - cheat gfi colorbox 1 0 0`

## General Stuff

* Campfire `cheat gfi campfire 1 0 0`
* Bed `cheat gfi bed_simple 1 0 0`
* Smithy `cheat gfi smithy 1 0 0`
* Tannery `cheat gfi tannery 1 0 0`
* Loom `cheat gfi loom 1 0 0`
* Forge `cheat gfi forge 1 0 0`
* Mortar and Pestle `cheat gfi pestle 1 0 0`
* Water Barrel `cheat gfi waterbarrel 1 0 0`
* Cooking Pot `cheat gfi cookingpot 1 0 0`
* Grill `cheat gfi grill 1 0 0`
* Preserving Bag `cheat gfi bag 1 0 0`
* Feeding Trough `cheat gfi trough 1 0 0`
* Wardrums `cheat gfi wardrums 1 0 0`

## Basics of Building

* Thatch Wall `cheat gfi wall_thatch 1 0 0`
* Thatch Roof `cheat gfi roof_thatch 1 0 0`
* Thatch Floor `cheat gfi floor_thatch 1 0 0`
* Thatch Door `cheat gfi door_small_thatch 1 0 0`
* Thatch Ceiling `cheat gfi ceiling_thatch 1 0 0`
* Wooden Chair `cheat gfi chair 1 0 0`
* Wooden Table `cheat gfi woodtable 1 0 0`
* Rope Ladder `cheat gfi rope 1 0 0`
* Portable Rope Ladder `cheat gfi portable 1 0 0`

## Secrets of Building

* Wood Roof `cheat gfi roof_wood 1 0 0`
* Wood Door `cheat gfi door_small_wood 1 0 0`
* Wood Floor `cheat gfi floor_wood 1 0 0`
* Wooden Ladder `cheat gfi ladder_static_wood 1 0 0`
* Wood Pillar `cheat gfi pillar_s_wood 1 0 0`
* Wood Ceiling `cheat gfi ceiling_wood 1 0 0`
* Wood Staircase `cheat gfi staircase_wood 1 0 0`
* Wood Wall `cheat gfi wall_wood 1 0 0`
* Small Wood Gate `cheat gfi gate_s_wood 1 0 0`
* Small Wood Gateway `cheat gfi gateway_s_wood 1 0 0`
* Large Wood Gate `cheat gfi gate_l_wood 1 0 0`
* Large Wood Gateway `cheat gfi gateway_l_wood 1 0 0`
* Wood Fence Support `cheat gfi fence_support_wood 1 0 0`
* Wall Hook Base `cheat gfi wallhook 1 0 0`
* Wood Gun Mount `cheat gfi gunmount_wood 1 0 0`
* Wood Cliff Platform `cheat gfi cliff_wood 1 0 0`
* Large Storage Box `cheat gfi storage 1 0 0`
* Lever `cheat gfi lever 1 0 0`
* Wooden Sign `cheat gfi woodsign 1 0 0`
* Wooden Billboard `cheat gfi woodsign_l 1 0 0`
* Wooden Wall Sign `cheat gfi woodsign_wall 1 0 0`
* Training Dummy `cheat gfi dummy 1 0 0`
* Wood Canvas `cheat gfi canvas_wood 1 0 0`

## Esotery of Building

* Stone Floor `cheat gfi floor_stone 1 0 0`
* Stone Ceiling `cheat gfi ceiling_stone 1 0 0`
* Stone Wall `cheat gfi wall_stone 1 0 0`
* Stone Staircase `cheat gfi staircase_stone 1 0 0`
* Stone Pillar `cheat gfi pillar_s_stone 1 0 0`
* Stone Roof `cheat gfi roof_stone 1 0 0`
* Stone Door `cheat gfi door_small_stone 1 0 0`
* Small Stone Gateway `cheat gfi gateway_s_stone 1 0 0`
* Small Stone Gate `cheat gfi gate_s_stone 1 0 0`
* Large Stone Gateway `cheat gfi gateway_l_stone 1 0 0`
* Large Stone Gate `cheat gfi gate_l_stone 1 0 0`
* Lighthouse `cheat gfi lighthouse 1 0 0`
* Wood Elevator System `cheat gfi elevator_primitive_wood 1 0 0`
* Stone Fence Support `cheat gfi fence_support_stone 1 0 0`
* Stone Cliff Platform `cheat gfi cliff_stone 1 0 0`
* Taxation Bank `cheat gfi taxation 1 0 0`

## Land Cannons

* Puckle `cheat gfi PrimalItemStructure_Puckle 1 0 0`
* Puckle Ammo `cheat gfi puckle 1 0 0`
* Mortar Ammo `cheat gfi mortar 1 0 0`

Couldnt find spawn command for the mortar cannon but u can find 100 mortar cannons + 100 Ammo in the cheatbox (`cheat gfi cheatbox 1 0 0`)

## Structures (Ship)

###  Planks / Gunports

* Small Wood Plank `cheat gfi Ship_Plank_Wood_small 1 0 0`
* Medium Wood Plank `cheat gfi Ship_Plank_Wood_medium 1 0 0`
* Large Wood Plank `cheat gfi Ship_Plank_Wood_large 1 0 0`
* Medium Gunport `cheat gfi Ship_Gunport_Wood_medium 1 0 0`
* Large Gunport `cheat gfi Ship_Gunport_Wood_large 1 0 0`

### Decks

* Small Ship Deck `cheat gfi ship_deck_wood_small 1 0 0`
* Medium Ship Deck `cheat gfi ship_deck_wood_medium 1 0 0`
* Large Ship Deck `cheat gfi ship_deck_wood_large 1 0 0`

### Sails

* Small Speed Sail `cheat gfi sail_small 1 0 0`
* Small Weight Sail `cheat gfi sail_small_weight 1 0 0`
* Small Handling Sail `cheat gfi sail_small_accel 1 0 0`
* Medium Speed Sail `cheat gfi sail_medium 1 0 0`
* Medium Weight Sail `cheat gfi sail_medium_weight 1 0 0`
* Medium Handling Sail `cheat gfi sail_medium_accel 1 0 0`
* Large Speed Sail `cheat gfi sail_large 1 0 0`
* Large Weight Sail `cheat gfi sail_large_weight 1 0 0`
* Large Handling Sail `cheat gfi sail_large_accel 1 0 0`

## Attachments and others

* Dinghy Ship Hangar `cheat gfi shiphangar_dinghy 1 0 0`
* Diving Attachment `cheat gfi diving 1 0 0`
* Steering Wheel `cheat gfi PrimalItemStructure_Ship_DriversSeat 1 0 0`

Resource Box

Ammo Box

Lieutenant Command Podium

Couldnt find the last 3 but u can get them from the Structure Cheatbox (`Cheat gfi cheatbox 1 0 0`).

## Cannons / Ammo

* Ship Cannon `cheat gfi shipcannon 1 0 0`
* Medium Cannon Ball `cheat gfi cannon 1 0 0`
* Explosive Barrel `cheat gfi barrel 1 0 0`
* Spike Shot `cheat gfi spike 1 0 0`
* Bar Shot `cheat gfi bar 1 0 0`
* Large Cannon `cheat gfi cannon_l 1 0 0`
* Large Cannon Ball `cheat gfi cannonball_l 1 0 0`
* Swivel Gun `cheat gfi swivel 1 0 0`
* Grape Shot `cheat gfi grapeshot 1 0 0`
* CanisterShot `cheat gfi canister 1 0 0`
* Liquid Flame `cheat gfi fire 1 0 0`

## Ships

### Dinghy

`cheat ssf dinghy`

### Raft

`cheat ssf raft`

or

`cheat SpawnShip "Blueprint'/Game/Atlas/Vehicles/Ships/Raft/Ship_BP_Raft.Ship_BP_Raft'" "Blueprint'/Game/Atlas/Structures/ShipHulls/ShipHull_BP_Raft.ShipHull_BP_Raft'" 10 true true true`

### "Ramshackle NPC" Sloop with 2x Small Speed Sail

`cheat SpawnShip "Blueprint'/Game/Atlas/Vehicles/Ships/Sloop/Ship_BP_Sloop_FromNPC.Ship_BP_Sloop_FromNPC'" "Blueprint'/Game/Atlas/Structures/ShipHulls/ShipHullSloop_BP_FromNPC.ShipHullSloop_BP_FromNPC'" 10 true true true`

### Sloop with 2x Small Speed Sail

`cheat ssf sloop`

or

`cheat SpawnShip "Blueprint'/Game/Atlas/Vehicles/Ships/Sloop/Ship_BP_Sloop.Ship_BP_Sloop'" "Blueprint'/Game/Atlas/Structures/ShipHulls/ShipHullSloop_BP.ShipHullSloop_BP'" 10 true true true`

### Schooner with 2x Medium Speed Sail

`cheat ssf schooner`

or

`cheat SpawnShip "Blueprint'/Game/Atlas/Vehicles/Ships/Schooner/Ship_BP_Schooner.Ship_BP_Schooner'" "Blueprint'/Game/Atlas/Structures/ShipHulls/ShipHullSchooner_BP.ShipHullSchooner_BP'" 10 true true true`

### Brigantine with 3x Large Speed Sail

`cheat ssf brigantine`

or

`cheat spawnbrig`

### Galleon with 6x Large Speed Sail

`cheat ssf galleon`

or

`cheat SpawnShip "Blueprint'/Game/Atlas/Vehicles/Ships/Galleon/Ship_BP_Galleon.Ship_BP_Galleon'" "Blueprint'/Game/Atlas/Structures/ShipHulls/ShipHullGalleon_BP.ShipHullGalleon_BP'" 10 true true true`

### Flying Airship (dont spawn on ground or it will crash your game)

`cheat ssf steampunk`

or

`cheat SpawnShip "Blueprint'/Game/Atlas/Devkit/Examples/Steampunk/Aircraft_BP_Steampunk.Aircraft_BP_Steampunk'" "Blueprint'/Game/Atlas/Devkit/Examples/Steampunk/ShipHull_BP_Steampunk.ShipHull_BP_Steampunk'" 10 true true true`

### Tank

`cheat ssf tank`

or

`cheat SpawnShip "Blueprint'/Game/Atlas/Devkit/Examples/Tank/Tank_BP.Tank_BP'" "Blueprint'/Game/Atlas/Devkit/Examples/Tank/ShipHull_BP_Tank.ShipHull_BP_Tank'" 10 true true true`

### Plane

`cheat ssf zephyr`

or

`cheat SpawnShip "Blueprint'/Game/Atlas/Devkit/Examples/Airplane/Aircraft_BP_Zephyr.Aircraft_BP_Zephyr'" "Blueprint'/Game/Atlas/Devkit/Examples/Airplane/ShipHull_BP_Hoopoe.ShipHull_BP_Hoopoe'" 10 true true true`
Wild Creatures with random level
Some have a chance to spawn as Alpha!

### Ghost Ship (Can only see it at nighttime on daytime its invisible) `cheat ssf ghostship`

## NPC

* Female NPC `cheat summon HumanNPC_BP_Crew_Female_C`
* Male NPC `cheat summon HumanNPC_BP_Crew_Male_C`
* Merchant Ship Ressource Vendor `cheat summon CommoditySellerNPC_MerchantShip_BP_C`
* Merchant Ship Cosmetics Vendor `cheat summon CosmeticSellerNPC_MerchantShip_BP_C`

## Only working in a Freeport

* Crew Recruiter `cheat summon CrewRecruiterNPC_BP_C (Spawns invisible but can be used)`
* Cosmetics Vendor `cheat summon CosmeticSellerNPC_BP_C (Spawns invisible but can be used)`
* Ferryman `cheat summon FerrymanNPC_BP_C (Spawns invisible but can be used)`
* Ressource Trade Vendor `cheat summon CommoditySellerNPC_Freeport_BP_C`

## Animals

* Anglerfish `cheat summon Anglerfish_Character_BP_Child_C`
* Bat `cheat summon CaveBat_Character_BP_C`
* Bear `cheat summon Bear_Character_BP_C`
* Cavebear `cheat summon Bear_Character_BP_Cave_C`
* Catfish `cheat summon FreshwaterFish_Catfish_C`
* Chicken `cheat summon Chicken_Character_BP_Child_C`
* Sea Bass `cheat summon SaltwaterFish_Cod_C`
* Cow `cheat summon Cow_Character_BP_C`
* Crocodile `cheat summon Crocodile_Character_BP_C`
* Crow `cheat summon Crow_Character_BP_C`
* Cyclops `cheat summon Cyclops_Character_BP_C`
* Dolphin `cheat summon Dolphin_Character_BP_Child_C`
* Dragon `cheat summon Draco_Character_BP_C`
* Eel `cheat summon AtlasEel_Character_BP_C`
* Elephant `cheat summon Elephant_Character_BP_C`
* Fire Elemental `cheat summon FireElemental_Character_BP_C`
* Freshwater Fish `cheat summon FreshwaterFish_Character_BP_C`
* Blue Whale (Gentle Whale) `cheat summon GentleWhale_Character_BP_C`
* Giant Ant `cheat summon GiantAnt_Character_BP_C`
* Giant Bee `cheat summon GiantBee_Character_BP_C`
* Giant Snake `cheat summon GiantSnake_Character_BP_C`
* Giraffe `cheat summon Giraffe_Character_BP_C`
* Gorgon (Medusa) `cheat summon Gorgon_Character_BP_C`
* Horse `cheat summon Horse_Character_BP_C`
* Hydra `cheat summon Hydra_Character_BP_C`
* Jellyfish `cheat summon Jellyfish_Character_BP_C`
* Kraken `cheat summon Kraken_Character_BP_C (doesnt work)`
* Leatherwing `cheat summon Leatherwing_Character_BP_C`
* Lion `cheat summon Lion_Character_BP_C`
* Manta Ray `cheat summon MantaRay_Character_BP_C`
* Marlin `cheat summon SaltwaterFish_Marlin_C`
* Sperm Whale (Mean Whale) `cheat summon MeanWhale_Character_BP_C`
* Mermaid `cheat summon Mermaid_Character_BP_C`
* Monkey `cheat summon Monkey_Character_BP_C`
* Ostrich `cheat summon Ostrich_Character_BP_C`
* Parrot `cheat summon Parrot_Character_BP_C`
* Penguin `cheat summon Penguin_Character_BP_C`
* Pig `cheat summon Pig_Character_BP_C`
* Rabbit `cheat summon Rabbit_Character_BP_C`
* Rattlesnake `cheat summon Rattlesnake_Character_BP_C`
* Razortooth `cheat summon Razortooth_Character_BP_C`
* Rhino `cheat summon WildRhino_Character_BP_C`
* Rock Elemental `cheat summon RockElemental_Character_BP_C`
* Saltwater Fish `cheat summon SaltwaterFish_Character_BP_C`
* Scorpion `cheat summon GiantScorpion_Character_BP_C`
* Seagull `cheat summon Seagull_Character_BP_C`
* Shark `cheat summon Shark_Character_BP_C`
* Sheep `cheat summon AtlasSheep_Character_BP_C`
* Shieldhorn `cheat summon ShieldHorn_Character_BP_C`
* Spider `cheat summon Spider_Character_BP_C`
* Squid `cheat summon Squid_Character_BP_C`
* Tiger `cheat summon Tiger_Character_BP_C`
* Tuna `cheat summon SaltwaterFish_Tuna_C`
* Turtle `cheat summon GiantTurtle_Character_BP_C`
* Vulture `cheat summon AtlasVulture_Character_BP_C`
* Wolf `cheat summon Wolf_Character_BP_C`
* Yeti `cheat summon Yeti_Character_BP_Child_C`

## Tamed Creatures level 45

For different level change 30 to any number you want. They get +50% level from gmsummon.

* Female NPC `cheat gmsummon "HumanNPC_BP_Crew_Female_C" 30`
* Male NPC `cheat gmsummon "HumanNPC_BP_Crew_Male_C" 30`

Only working for "tameable" creatures others have to be tamed via "`cheat forcetame" from wild.`

* Bear `cheat gmsummon "Bear_Character_BP_C" 30`
* Chicken `cheat gmsummon "Chicken_Character_BP_Child_C" 30`
* Cow `cheat gmsummon "Cow_Character_BP_C" 30`
* Crow `cheat gmsummon "Crow_Character_BP_C" 30`
* Dragon `cheat gmsummon "Draco_Character_BP_C" 30`
* Elephant `cheat gmsummon "Elephant_Character_BP_C" 30`
* Giraffe `cheat gmsummon "Giraffe_Character_BP_C" 30`
* Horse `cheat gmsummon "Horse_Character_BP_C" 30`
* Lion `cheat gmsummon "Lion_Character_BP_C" 30`
* Monkey `cheat gmsummon "Monkey_Character_BP_C" 30`
* Ostrich `cheat gmsummon "Ostrich_Character_BP_C" 30`
* Parrot `cheat gmsummon "Parrot_Character_BP_C" 30`
* Penguin `cheat gmsummon "Penguin_Character_BP_C" 30`
* Pig `cheat gmsummon "Pig_Character_BP_C" 30`
* Rabbit `cheat gmsummon "Rabbit_Character_BP_C" 30`
* Razortooth `cheat gmsummon "Razortooth_Character_BP_C" 30`
* Rhino `cheat gmsummon "WildRhino_Character_BP_C" 30`
* Seagull `cheat gmsummon "Seagull_Character_BP_C" 30`
* Sheep `cheat gmsummon "AtlasSheep_Character_BP_C" 30`
* Shieldhorn `cheat gmsummon "ShieldHorn_Character_BP_C" 30`
* Tiger `cheat gmsummon "Tiger_Character_BP_C" 30`
* Vulture `cheat gmsummon "AtlasVulture_Character_BP_C" 30`
* Wolf `cheat gmsummon "Wolf_Character_BP_C" 30`

### Skins

* Aberrant Helmet Skin `cheat gfi PrimalItemSkin_Shoppable 1 0 0`
* Test Bear Saddle Skin `cheat gfi PrimalItemSkin_BearSkinTest 1 0 0`
* Kraken Visage Skin `cheat gfi PrimalItemSkin_KrakenHat 1 0 0`
* Bandana Skin `cheat gfi PrimalItemSkin_CrewBandanna 1 0 0`
* Eye Patch Skin `cheat gfi PrimalItemSkin_EyePatch 1 0 0`
* Top Hat Skin `cheat gfi PrimalItemSkin_TopHat 1 0 0`
* Officer's Hat Skin `cheat gfi PrimalItemSkin_OfficerHat 1 0 0`
* Officer's Coat Skin `cheat gfi PrimalItemSkin_OfficerShirt 1 0 0`
* Officer's Gloves Skin `cheat gfi PrimalItemSkin_OfficerGloves 1 0 0`
* Officer's Pants Skin `cheat gfi PrimalItemSkin_OfficerPants 1 0 0`
* Officer's Boots Skin `cheat gfi PrimalItemSkin_OfficerBoots 1 0 0`

### Dyes

* Color `Cheatbox Cheat gfi colorbox 1 0 0`

Has 100x every Color in the game + 100x Paintbrush and 100x Spray Painter

* Spray Painter `cheat gfi spray 1 0 0`
* Paintbrush `cheat gfi brush 1 0 0`
* Magenta Coloring `cheat gfi actually 1 0 0`
* ALizarin Red Coloring `cheat gfi alizarin 1 0 0`
* Aubergine Brown Coloring `cheat gfi aubergine 1 0 0`
* Black Coloring `cheat gfi black 1 0 0`
* Blackberry Coloring cheat gfi blackberry 1 0 0`
* Blackcurrant Coloring `cheat gfi blackcurrant 1 0 0`
* Blue Coloring `cheat gfi blue 1 0 0`
* Blue Whale Coloring `cheat gfi bluewhale 1 0 0`
* Brick Coloring `cheat gfi brick 1 0 0`
* Bright Red Coloring `cheat gfi brightred 1 0 0`
* Brown Coloring `cheat gfi brown 1 0 0`
* Cantaloupe Coloring `cheat gfi cantaloupe 1 0 0`
* Cerulean Coloring `cheat gfi cerulean 1 0 0`
* Chateau Green Coloring `cheat gfi chateaugreen 1 0 0`
* Conifer Green Coloring `cheat gfi confiergreen 1 0 0`
* Cyan Coloring `cheat gfi cyan 1 0 0`
* Dark Pink Coloring `cheat gfi darkpink 1 0 0`
* Forest Coloring `cheat gfi forest 1 0 0`
* Gold Coloring `cheat gfi gold 1 0 0`
* Green Coloring `cheat gfi green 1 0 0`
* Indigo Coloring `cheat gfi indigo 1 0 0`
* Light Sea Green Coloring `cheat gfi lightseagreen 1 0 0`
* Madang Green Coloring `cheat gfi madanggreen 1 0 0`
* Purple Coloring `cheat gfi magenta 1 0 0`
* Mango Coloring `cheat gfi mango 1 0 0`
* Mineral Gray Coloring `cheat gfi mineralgray 1 0 0`
* Mud Coloring `cheat gfi mud 1 0 0`
* Napa Brown Coloring `cheat gfi napabrown 1 0 0`
* Navy Coloring `cheat gfi navy 1 0 0`
* Olive Coloring `cheat gfi olive 1 0 0`
* Orange Coloring `cheat gfi orange 1 0 0`
* Orange Peel Coloring `cheat gfi orangepeel 1 0 0`
* Orient Blue Coloring `cheat gfi orientblue 1 0 0`
* Paco Brown Coloring `cheat gfi pacobrown 1 0 0`
* Parchment Coloring `cheat gfi parchment 1 0 0`
* Pink Coloring `cheat gfi pink 1 0 0`
* Pohutakawa Red Coloring `cheat gfi pohutukawared 1 0 0`
* Purple Coloring `cheat gfi purple 1 0 0`
* Red Coloring `cheat gfi red 1 0 0`
* Royalty Coloring `cheat gfi royalty 1 0 0`
* Selective Yellow Coloring `cheat gfi selectiveyellow 1 0 0`
* Shalimar Yellow Coloring `cheat gfi shalimar 1 0 0`
* Silver Coloring `cheat gfi silver 1 0 0`
* Sky Coloring `cheat gfi sky 1 0 0`
* Slate Coloring `cheat gfi slate 1 0 0`
* Snow White Coloring `cheat gfi snowwhite 1 0 0`
* Swamp Purple Coloring `cheat gfi swampypurple 1 0 0`
* Tan Coloring `cheat gfi tan 1 0 0`
* Tangerine Coloring `cheat gfi tangerine 1 0 0`
* Watercourse Green Coloring `cheat gfi watercoursegreen 1 0 0`
* White Coloring `cheat gfi white 1 0 0`
* Yellow Coloring `cheat gfi yellow 1 0 0`

I found T2 somewhere and used it to work out T1 and T3. All these commands work.

* `cheat gfi saddlegeneric 1 0 0 T1 Saddle`
* `cheat gfi saddlegeneric_tier2 1 0 0 T2 Saddle`
* `cheat gfi saddlegeneric_tier3 1 0 0 T3 Saddle`

the Resource Box is `cheat gfi paymentbox 1 0 0`

Found Ammo container for you

`cheat gfi PrimalItemStructure_AmmoContainer 1 0 0`

`admincheat SpawnDino "Blueprint'/Game/Atlas/Creatures/Kraken/Kraken_Character_BP.Kraken_Character_BP'" 5 0 0 35` its funcional !!

`cheat gfi PrimalItemStructure_Ship_DriversSeat 1 0 0`

https://docs.google.com/document/d/1bPimZRac3nKBg63DebR-fayli8_8i3QF-zSuopXen7Q/edit?usp=sharing
