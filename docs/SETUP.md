# Server Setup

## Server Portmap

Typically, games that use the
[Valve Server Query](https://developer.valvesoftware.com/wiki/Server_queries)
protocol use 27015 for the query port, with 7777 and 7778 being the client data
ports.

To simplify the port-forwarding, I've crammed them into consecutive ports.
Each server instance needs 5 ports. The seamless port is specific to ATLAS.
Every server has a dedicated seamless port (which is only configured in the
`ServerGrid.json`).

The 3x3 grid is hosted on three physical servers (Dell PowerEdge 610 w/96GB RAM).

Ports are configured to allow for simple port-forwarding rules
(just forward the range for TCP/UDP):

    55500-55514 -> qws217 (A1, B1, C1)
    55515-55529 -> app85  (A2, B2, C2)
    55530-55544 -> app72  (A3, B3, C3)


| Server | Machine | Query | Seamless | Client 1 | Client 2 | RCON  |
|--------|---------|-------|----------|----------|----------|-------|
| A1     | qws217  | 55500 | 55501    | 55502    | 55503    | 55504 |
| B1     | qws217  | 55505 | 55506    | 55507    | 55508    | 55509 |
| C1     | qws217  | 55510 | 55511    | 55512    | 55513    | 55514 |
| A2     | app85   | 55515 | 55516    | 55517    | 55518    | 55519 |
| B2     | app85   | 55520 | 55521    | 55522    | 55523    | 55524 |
| C2     | app85   | 55525 | 55526    | 55527    | 55528    | 55529 |
| A3     | app72   | 55530 | 55531    | 55532    | 55533    | 55534 |
| B3     | app72   | 55535 | 55536    | 55537    | 55538    | 55539 |
| C3     | app72   | 55540 | 55541    | 55542    | 55543    | 55544 |

## Setup Notes

Clone this repository, it's expected you're doing this as user `steam`, from
steam's home directory (`/home/steam`):

```bash
git clone git@gitlab.com:waterark/seven-seas.git
```

ServerAdminPassword configuration is obviously *not* part of this repository.

Create `~/.atlasmanager.cfg` on each machine and add:

```
atlas_ServerAdminPassword="keyboardcat"
```

Install [SteamCMD](https://developer.valvesoftware.com/wiki/SteamCMD#Linux) via:

```bash
# This is Debian/Ubuntu specific...
# Dependencies
# lib32gcc1 is for steam
# jq and redis-tools are for atlasmanager
sudo apt-get install lib32gcc1 jq redis-tools
# Create steam user
useradd -m steam
# Switch to steam user
sudo -iu steam
# Download and install steamcmd
mkdir -p ~/Steam && cd ~/Steam && curl -sqL "https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz" | tar zxvf - && ./steamcmd.sh +quit
```

Install [atlas-server-tools](https://github.com/BoiseComputer/atlas-server-tools/):

```bash
curl -sL http://git.io/fh4HA | sudo bash -s steam
```

Replace `/etc/atlasmanager` with a symlink to the pre-configured stuff in this
repository:

```bash
sudo rm -rf /etc/atlasmanager # delete the old configs
sudo ln -s /home/steam/seven-seas/atlasmanager /etc/atlasmanager
```

Enable the right instances to run on this host by symlinking from
`atlasmanager/available` to `atlasmanager/instances`:

```bash
# run from ~/
# for qws217
ln -s ../available/a1.cfg seven-seas/atlasmanager/instances/a1.cfg
ln -s ../available/b1.cfg seven-seas/atlasmanager/instances/b1.cfg
ln -s ../available/c1.cfg seven-seas/atlasmanager/instances/c1.cfg
# for app85
ln -s ../available/a2.cfg seven-seas/atlasmanager/instances/a2.cfg
ln -s ../available/b2.cfg seven-seas/atlasmanager/instances/b2.cfg
ln -s ../available/c2.cfg seven-seas/atlasmanager/instances/c2.cfg
# for app72
ln -s ../available/a3.cfg seven-seas/atlasmanager/instances/a3.cfg
ln -s ../available/b3.cfg seven-seas/atlasmanager/instances/b3.cfg
ln -s ../available/c3.cfg seven-seas/atlasmanager/instances/c3.cfg
```

Have arkmanager install ARK (only needs to be run for one instance per host,
since the installation itself is shared):

```bash
arkmanager install @a1
```

Symlink `ServerGrid.json` and create `ServerGrid.ServerOnly.json`.

```bash
cd ~steam

# Wire up server config
ln -s /home/steam/seven-seas/ShooterGame/ServerGrid.json ATLAS/ShooterGame/
ln -s /home/steam/seven-seas/ShooterGame/ServerGrid.ServerOnly.json ATLAS/ShooterGame/

# Wire up admin config
mkdir -p ATLAS/ShooterGame/Saved
ln -s /home/steam/seven-seas/ShooterGame/Saved/AllowedCheaterSteamIDs.txt ATLAS/ShooterGame/Saved/AllowedCheaterSteamIDs.txt
```

Then start them all:

```bash
arkmanager start @all
```

Be sure to edit crontab ("`crontab -e`"), and tell atlasmanager to check for updates every hour:

```
# Check for updates every hour. Will also restart any crashed servers at that time.
0 * * * * /usr/local/bin/atlasmanager --cronjob update --warn --saveworld --backup @all  --args  -- >/dev/null 2>&1
# Backup files every night at 3.
0 3 * * * /usr/local/bin/atlasmanager backup @all
```
