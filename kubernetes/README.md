# Kubernetes deployment

This directory isn't relevant to anyone but me, unless you're interested in how
I set up the infrastructure, want to set it up the same, and happen to be using
Kubernetes.

ARK/ATLAS is installed "natively", although it would be insanely cool to have
a [Kubernetes Operator](https://coreos.com/operators/) that introduces a
[Custom Resource](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/)
(CRD), which then allows you to define a grid to automatically roll out the servers
in containers, along with a redis cluster and monitoring, metrics, and alerts.
It would have to parse the `ServerGrid.json`, map servers to the various hosts,
work out the portmapping, including whether there's NAT involved, and spit out
a processed `ServerGrid.json` (and ideally a set of importable pfsense rules).

Something as easy as:

```yml
kind: AtlasGrid
api: arkbrowser.com/v1
metadata:
  name: seven-seas
  namespace: arkbrowser
spec:
  selector:
    matchLabels:
      host: atlas
  map:
  - name: grid
    configMap:
      key: ServerGrid.json
    defaultMode: 420
```

but I digress...

## These YML Files

The `.yml` files in this directory are to deploy:

* Redis (which ATLAS uses as its shared DB across the cluster).
* process-exporter (which allows Prometheus to get ARK and ATLAS process details).
* Prometheus (which monitors ARK and ATLAS).

The configuration is particular to my setup as to which nodes host what, so running
the `.yml` files against your own kubernetes cluster as-is will probably not get
much to work.

### Improvements that should be done

* [redis-exporter](https://github.com/oliver006/redis_exporter/blob/master/contrib/k8s-redis-and-exporter-deployment.yaml)
  should be added as a sidecar to redis.
  * then, update prometheus to include redis-exporter as well.
