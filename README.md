# Atlas - Seven Seas

This repository contains everything needed to put together the
`WaterARK - Seven Seas` Atlas server cluster.

<img src="ShooterGame/ServerGrid/MapImg.jpg" alt="3x3 Atlas World Map" width="500"/>

Atlas itself is managed via
[atlas-server-tools](https://github.com/BoiseComputer/atlas-server-tools) (aka `atlasmanager`).

Originally the plan was to follow [drac346's tutorial](https://www.drac346.net/2018/12/atlas-dedicated-server-setup-main-menu.html). The site has since moved to [krookedskull.com](https://krookedskull.com/index.php?title=AtlasDSSGuide).

The `ServerGrid.json` and image files are mostly based on NaCHO's map:

* Full Quest Line to defeat the kraken
* Includes spawns for mermaids/gentle whales
* All resources can be found!
* All islands are named and have Discoveries/XP
* Ghost Ship and NPC trader ship Routes (marked on map)
* Custom Map images for a much more authentic experience


## Documentation

* [Setup Notes](docs/SETUP.md)
* [Admin Cheat Codes](docs/ADMINCHEATS.md)

## Links

* [Chromecast Dashboard Sender](https://boombatower.github.io/chromecast-dashboard/sender/) (for grafana)